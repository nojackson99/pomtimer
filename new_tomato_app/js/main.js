//----------------------------------------------------main.js------------------------------------------------------
// This file holds imports of all other js files and css styling file. Imported into html file index.html
//-----------------------------------------------------------------------------------------------------------------


// ------------HTML & CSS todo's---------------
// [X] todo: show active task below start focus button
// [X] todo: settings button that opens modal (maybe in top right?)
// [X] todo: profie button that allows for switching to new profile or creating a new one (maybe top left)
// [] todo: implement CSS for settings and new profile modals


// -------------- IMPORTS ---------------------
import * as Data from './data.js';        //data associated code
import * as Header from './header.js';    //header associated code
import * as Tasks from './tasks.js';      //task associated code
import * as Theme from './theme.js';      //theme associated code
import * as MyTimer from './timer.js';    //timer associated code
import * as Storage from './storage.js';  //storage associated code
